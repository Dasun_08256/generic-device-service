For MySQL

    properties file
    
        #### MySQL configuration ######### UnComment these
        spring.jpa.hibernate.ddl-auto=update
        spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5Dialect
        
        spring.c3po.driver-class=com.mysql.jdbc.Driver
        spring.c3po.jdbc-url=jdbc:mysql://localhost:3306/iot_generic_device?useSSL=false
        spring.c3po.user=root
        spring.c3po.password=root
        
        
        #### Mongo DB configuration #########  comment or remmove these
        #spring.data.mongodb.repositories.type=auto
        #spring.data.mongodb.username=root
        #spring.data.mongodb.password=root
        #spring.data.mongodb.uri=mongodb://172.17.0.1:27017/generic_device_db
        
    Application.java file
    
        UnComment this 
    
        /*@Bean
            @ConfigurationProperties("spring.c3po")
            public ComboPooledDataSource dataSource() {
                return new ComboPooledDataSource();
            }*/
            
For MongoDb

    properties file
    
        #### MySQL configuration ######### Comment or remove these
        #spring.jpa.hibernate.ddl-auto=update
        #spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5Dialect
        
        #spring.c3po.driver-class=com.mysql.jdbc.Driver
        #spring.c3po.jdbc-url=jdbc:mysql://localhost:3306/iot_generic_device?useSSL=false
        #spring.c3po.user=root
        #spring.c3po.password=root
        
        
        #### Mongo DB configuration #########  UnComment this
        spring.data.mongodb.repositories.type=auto
        spring.data.mongodb.username=root
        spring.data.mongodb.password=root
        spring.data.mongodb.uri=mongodb://172.17.0.1:27017/generic_device_db
        
    Application.java file
    
        Comment or remove these
    
        /*@Bean
            @ConfigurationProperties("spring.c3po")
            public ComboPooledDataSource dataSource() {
                return new ComboPooledDataSource();
            }*/

There are sample CRUD APIS for both MySQL and MongoDB configurations