package lk.dialog.iot.gds.service.impl;

import lk.dialog.iot.gds.exception.GenericDeviceException;
import lk.dialog.iot.gds.model.GenericDevice;
import lk.dialog.iot.gds.model.GenericDeviceMongo;
import lk.dialog.iot.gds.repositary.GenericDeviceRepository;
import lk.dialog.iot.gds.repositary.MongoGenericDeviceRepository;
import lk.dialog.iot.gds.service.GenericDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Optional;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class GenericDeviceServiceImpl implements GenericDeviceService {

    @Autowired
    private GenericDeviceRepository genericDeviceRepository;

    @Autowired
    private MongoGenericDeviceRepository reactiveGenericDeviceRepository;

    @Override
    public Collection<GenericDevice> findAll() {

        Collection<GenericDevice> genericDevices = genericDeviceRepository.findAll();
        return genericDevices;
    }

    @Override
    public GenericDevice findOne(int id) throws GenericDeviceException {
        Optional<GenericDevice> genericDevice = genericDeviceRepository.findById(id);
        return genericDevice.orElseThrow(()-> new GenericDeviceException("Cannot find Generic Device"));
    }

    @Override
    public GenericDevice create(GenericDevice genericDevice) {
        if (genericDevice.getId() != 0) {
            return null;
        }

        GenericDevice savedGenericDevice = genericDeviceRepository.save(genericDevice);
        return savedGenericDevice;
    }

    @Override
    public GenericDevice update(Integer id, GenericDevice genericDevice) {
        genericDevice.setId(id);
        return genericDeviceRepository.save(genericDevice);
    }

    @Override
    public void delete(Integer id) {
        genericDeviceRepository.deleteById(id);
    }

    @Override
    public Flux<GenericDeviceMongo> getGenericDevices() {
        Flux<GenericDeviceMongo> genericDevices = reactiveGenericDeviceRepository.findAll();
        return genericDevices;
    }

    @Override
    public Mono<GenericDeviceMongo> createGenericDevices(@Valid GenericDeviceMongo deviceMongo) {
        return reactiveGenericDeviceRepository.insert(deviceMongo);
    }


    @Override
    public Mono<GenericDeviceMongo> getGenericDeviceMongoById(String id) {
        return reactiveGenericDeviceRepository.findById(id);
    }

    @Override
    public Mono<GenericDeviceMongo> updateGenericDeviceMongo(String id, @Valid GenericDeviceMongo deviceMongo) {

        return reactiveGenericDeviceRepository.findById(id).flatMap(existingGenericDeviceMongo -> {
            existingGenericDeviceMongo.setDeviceId(deviceMongo.getDeviceId());
            return reactiveGenericDeviceRepository.save(existingGenericDeviceMongo);
        });
    }

    @Override
    public Mono<Void> deleteGenericDeviceMongo(String deviceId) {
        return reactiveGenericDeviceRepository.findById(deviceId)
                .flatMap(existingGenericDeviceMongo -> reactiveGenericDeviceRepository.delete(existingGenericDeviceMongo));
    }

    @Override
    public Flux<GenericDeviceMongo> streamAllGenericDeviceMongos() {
        return reactiveGenericDeviceRepository.findAll();
    }
}
