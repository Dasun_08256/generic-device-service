package lk.dialog.iot.gds.service;

import lk.dialog.iot.gds.exception.GenericDeviceException;
import lk.dialog.iot.gds.model.GenericDevice;
import lk.dialog.iot.gds.model.GenericDeviceMongo;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.Collection;

public interface GenericDeviceService {

    Collection<GenericDevice> findAll();

    GenericDevice findOne(int id) throws GenericDeviceException;

    GenericDevice create(GenericDevice genericDevice);

    GenericDevice update(Integer id, GenericDevice genericDevice);

    void delete(Integer id);

    Flux<GenericDeviceMongo> getGenericDevices();

    Mono<GenericDeviceMongo> createGenericDevices(@Valid GenericDeviceMongo deviceMongo);

    Mono<GenericDeviceMongo> getGenericDeviceMongoById(String id);

    Mono<GenericDeviceMongo> updateGenericDeviceMongo(String id, @Valid GenericDeviceMongo deviceMongo);

    Mono<Void> deleteGenericDeviceMongo(String id);

    Flux<GenericDeviceMongo> streamAllGenericDeviceMongos();
}
