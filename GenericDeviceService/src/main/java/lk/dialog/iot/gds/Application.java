package lk.dialog.iot.gds;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class Application {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	/*Comment this bean creation for Mongodb*/
    /*@Bean
    @ConfigurationProperties("spring.c3po")
    public ComboPooledDataSource dataSource() {
        return new ComboPooledDataSource();
    }*/

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

}
