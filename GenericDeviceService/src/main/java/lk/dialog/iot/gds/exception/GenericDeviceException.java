package lk.dialog.iot.gds.exception;

public class GenericDeviceException extends Exception {

    private static final long serialVersionUID = 1L;

    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public GenericDeviceException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    public GenericDeviceException() {
        super();
    }
}
