package lk.dialog.iot.gds.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lk.dialog.iot.gds.exception.ErrorResponse;
import lk.dialog.iot.gds.exception.GenericDeviceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BaseController {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

   /* @Autowired
    protected ModelMapper modelMapper;*/

    @Autowired
    private ObjectMapper objectMapper;

    @ExceptionHandler(GenericDeviceException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(1000);
        error.setDesc(ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.OK);
    }

}
