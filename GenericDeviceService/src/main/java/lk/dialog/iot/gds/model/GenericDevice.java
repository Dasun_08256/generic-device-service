package lk.dialog.iot.gds.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "generic_device")
public class GenericDevice implements Serializable {

    private static final long serialVersionUID = -5679632916334924397L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int deviceId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }
}
