package lk.dialog.iot.gds.repositary;

import lk.dialog.iot.gds.model.GenericDeviceMongo;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MongoGenericDeviceRepository extends ReactiveMongoRepository<GenericDeviceMongo, String> {
}
