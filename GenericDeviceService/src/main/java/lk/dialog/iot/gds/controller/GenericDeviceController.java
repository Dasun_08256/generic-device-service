package lk.dialog.iot.gds.controller;

import lk.dialog.iot.gds.exception.GenericDeviceException;
import lk.dialog.iot.gds.model.GenericDevice;
import lk.dialog.iot.gds.model.GenericDeviceMongo;
import lk.dialog.iot.gds.service.GenericDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collection;

@RestController
public class GenericDeviceController extends BaseController {

    @Autowired
    GenericDeviceService genericDeviceService;

    /*My SQL CRUD operations*/

    @RequestMapping(value = "/api/genericDevice", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<GenericDevice>> getGenericDevices(HttpServletRequest request) throws GenericDeviceException {
        Collection<GenericDevice> genericDevices = genericDeviceService.findAll();
        return new ResponseEntity<>(genericDevices, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/genericDevice/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericDevice> getGenericDeviceById(@PathVariable("id") int id,HttpServletRequest request) throws GenericDeviceException {
        GenericDevice genericDevice = genericDeviceService.findOne(id);
        if (genericDevice == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(genericDevice, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/genericDevice", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericDevice> createGenericDevices(@RequestBody GenericDevice genericDevice,HttpServletRequest request) throws GenericDeviceException {
        GenericDevice savedGenericDevice = genericDeviceService.create(genericDevice);
        return new ResponseEntity<>(savedGenericDevice, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/genericDevice/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericDevice> updateGenericDevice(@PathVariable("id") Integer id, @RequestBody GenericDevice genericDevice,HttpServletRequest request) {

        GenericDevice updatedGenericDevice = genericDeviceService.update(id, genericDevice);

        if (updatedGenericDevice == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(updatedGenericDevice, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/genericDevice/{id}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericDevice> deleteGenericDevice(@PathVariable("id") Integer id,HttpServletRequest request) {
        genericDeviceService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /*MongoDB CRUD operations*/

    @GetMapping("/api/mongo/genericDevice")
    public Flux<GenericDeviceMongo> getGenericDevices() {
        return genericDeviceService.getGenericDevices();
    }

    @PostMapping("/api/mongo/genericDevice")
    public Mono<GenericDeviceMongo> createGenericDevices(@Valid @RequestBody GenericDeviceMongo deviceMongo) {
        return genericDeviceService.createGenericDevices(deviceMongo);
    }

    @GetMapping("/api/mongo/genericDevice/{id}")
    public Mono<ResponseEntity<GenericDeviceMongo>> getGenericDeviceMongoById(@PathVariable(value = "id") String id) {
        return genericDeviceService.getGenericDeviceMongoById(id).map(savedGenericDeviceMongo -> ResponseEntity.ok(savedGenericDeviceMongo))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PutMapping("/api/mongo/genericDevice/{id}")
    public Mono<ResponseEntity<GenericDeviceMongo>> updateGenericDeviceMongo(@PathVariable(value = "id") String id,
                                                                 @Valid @RequestBody GenericDeviceMongo deviceMongo) {
        return genericDeviceService.updateGenericDeviceMongo(id, deviceMongo)
                .map(updatedGenericDeviceMongo -> new ResponseEntity<>(updatedGenericDeviceMongo, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/api/mongo/genericDevice/{id}")
    public Mono<ResponseEntity<Void>> deleteGenericDeviceMongo(@PathVariable(value = "id") String id) {

        return genericDeviceService.deleteGenericDeviceMongo(id).then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK)))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping(value = "/api/stream/mongo/genericDevice", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<GenericDeviceMongo> streamAllGenericDeviceMongos() {
        return genericDeviceService.streamAllGenericDeviceMongos();
    }
}
