package lk.dialog.iot.gds.repositary;

import lk.dialog.iot.gds.model.GenericDevice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenericDeviceRepository extends JpaRepository<GenericDevice, Integer> {
}
